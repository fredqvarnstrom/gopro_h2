#!/usr/bin/env python
"""

H2 : Com Tech TL 2
    wester22

H3 : Com-Tech-TL10
    wester110

"""
import httplib
import socket
import struct
import ftplib
import subprocess
from ftplib import FTP
import os, time, sys, glob, re, shutil
import urllib2
import logging
import datetime
import url_H2
import url_H4
import utils
import config
import boto
from boto.s3.key import Key
import socket

# ###########################    Initialization   ################################################

ftp_server_ip = config.FTP_SERVER_IP
ftp_id = config.FTP_ID
ftp_pwd = config.FTP_PWD

logging_level = int(utils.get_param_from_xml('LOG_LEVEL'))

aws_bucket = utils.get_param_from_xml('AWS_BUCKET_NAME')
aws_key = utils.get_param_from_xml('AWS_KEY')
aws_security = utils.get_param_from_xml('AWS_SECRET')
host_name = socket.gethostname()

time_now = datetime.datetime.now()
log_file_name = '/home/gopro/' + time_now.strftime("%Y-%m-%d") + ".txt"


class GoProCTRL:

    info = None         # contains all info about camera
    c_type = None       # Camera type,    H2 or H3 or H4
    jpg_dir = ''

    url_base = None
    aws_key_path = ''

    def __init__(self, num):
        self.info = config.GoPro_List[num]
        self.c_type = self.info.get('type')
        if self.c_type == "H4":
            self.url_base = url_H4
        else:
            self.url_base = url_H2

        self.jpg_dir = '/home/gopro/' + self.info.get('name') + "/"
        if not os.path.exists(self.jpg_dir):
            os.makedirs(self.jpg_dir)
        self.aws_key_path = host_name + "/" + self.info.get('name') + "/"

    def wake(self):
        print "wake up GoPro..."
        logging.info("wake up GoPro...")
        if self.c_type == "H4":
            return self.send_cmd(self.url_base.url_shutter_on)
        else:
            return self.send_cmd(self.url_base.url_gopro_on)

    def send_cmd(self, cmd):
        if self.c_type == "H4":
            url = config.GOPRO_URL + cmd
        else:
            url = config.GOPRO_URL + cmd.format(self.info.get('pwd'))

        print "Sending command,   ", url

        try:
            result = urllib2.urlopen(url).read()
            return True
        except urllib2.HTTPError, e:
            logging.error('HTTPError = ' + str(e.code))
            return False
        except urllib2.URLError, e:
            logging.error('URLError = ' + str(e.reason))
            return False
        except httplib.HTTPException, e:
            logging.error('HTTPException')
            return False
        except Exception:
            import traceback
            logging.error('Generic exception: ' + traceback.format_exc())
            return False

    def sleep(self):
        print "- sleep"
        return self.send_cmd(self.url_base.url_gopro_off)

    def takepic(self):
        print "- take a photo"
        logging.info("- take a photo")
        if not self.send_cmd(self.url_base.url_mode_photo):
            return False
        utils.count_down(5)  # wait for photo mode to turn on
        if not self.send_cmd(self.url_base.url_shutter_on):
            return False
        utils.count_down(1)  # wait for photo to be taken

    def set_date_time(self):
        print "setting date and time..."
        logging.info("setting date and time...")
        s = time.strftime("%y%%%m%%%d%%%H%%%M%%%S", time.localtime())
        url = self.url_base.url_set_date_time + s
        return self.send_cmd(url)

    def download(self, last=True):
        print "- download last one"
        try:
            logging.info("- download last one")
            url = config.GOPRO_URL + config.url_media

            result = urllib2.urlopen(url).read()
            dirs = re.findall('href="(\d\d\dGOPRO)/"', result)
            if not dirs:
                print "No Media Folders"
                logging.error("No Media Folders")
                return None

            url += "/" + dirs[-1]
            result = urllib2.urlopen(url).read()
            pics = re.findall('href="(GOPR\d+\.JPG)"', result)
        except urllib2.URLError as e:
            logging.error(e)
            print e
            return None

        if not pics:
            print "No Pictures"
            logging.error("No Pictures")
            return None

        def download_pic(url, pic):
            url += "/" + pic
            result = urllib2.urlopen(url)
            print "Downloading %s (%s bytes)..." % (pic, result.headers['content-length'])
            logging.info("Downloading %s (%s bytes)..." % (pic, result.headers['content-length']))
            dir_path = '/home/gopro/' + self.info.get('name') + "/"

            download_file_name = dir_path + pic
            with open(download_file_name, "wb") as f:
                while True:
                    chunk = result.read(16 * 1024)
                    if not chunk:
                        break
                    f.write(chunk)
                    print f.tell(), "\r",
                    sys.stdout.flush()
            return download_file_name

        if last:
            return download_pic(url, max(pics))
        else:
            return [download_pic(url, pic) for pic in pics]

    def delete(self, last=True):
        print "- delete"
        logging.info("- delete")
        return self.send_cmd(self.url_base.url_delete_last if last else self.url_base.url_delete_all)

    def upload_file(self, photo_file_name, dr=None):
        try:
            p_file = open(photo_file_name, 'rb')
        except IOError as e:
            print 'Cannot upload file. Maybe file is corrupt or damaged'
            return False

        try:
            print 'File uploading : ' + photo_file_name
            logging.info('Uploading file : ' + photo_file_name)
            # insert exception code when failed to connect to the FTP server
            ftp = FTP(ftp_server_ip)  # the domain name is "ftp.comtechnology.co.nz"
            # insert exception code when failed to log in to the FTP server
            ftp.login(ftp_id, ftp_pwd)

            if dr is not None:
                if self.info.get('name') not in ftp.nlst():
                    ftp.mkd(self.info.get('name'))
                ftp.cwd(self.info.get('name'))

            # can change the file name when uploading
            ftp.storbinary('STOR ' + photo_file_name.split('/')[-1], p_file)
            print "STORing File now..."
            logging.info("STORing File now...")
            ftp.quit()
            p_file.close()
            print "File transfered"
            logging.info("File transfered")
            logging.info("")
            return True
        except ftplib.all_errors, e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False

    def sync_img(self):
        try:
            ftp = FTP(ftp_server_ip)  # the domain name is "ftp.comtechnology.co.nz"
            ftp.login(ftp_id, ftp_pwd)
            ftp.cwd(self.info.get('name'))

            l_list = glob.glob("/home/gopro/" + self.info.get('name') + "/*.JPG")
            local_list = []
            for f in l_list:
                buf = f.split('/')
                local_list.append(buf[-1])

            p_list = self.get_remote_file_list(self.aws_key_path)

            remote_list = [os.path.basename(f_path) for f_path in p_list]

            print "local_list : ", local_list
            print "remote_list : ", remote_list

            # if picture does not exist on AWS S3, upload it
            for f in local_list:
                if f not in remote_list:
                    print "file missed, uploading ... " + f

                    self.upload_file('/home/gopro/' + self.info.get('name') + "/" + f, True)

            # now, all pictures are uploaded, so delete all
            for f in local_list:
                utils.delete_file('/home/gopro/' + self.info.get('name') + "/" + f)

            return True
        except ftplib.all_errors, e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False

    def sync_with_aws(self):
        try:
            l_list = glob.glob("/home/gopro/" + self.info.get('name') + "/*.JPG")
            local_list = [os.path.basename(f_path) for f_path in l_list]

            l_a_list = self.get_remote_file_list(self.aws_key_path)
            remote_list = [os.path.basename(f_path) for f_path in l_a_list]

            print "local_list : ", local_list
            print "remote_list : ", remote_list

            # if picture does not exist on FTP server, upload it
            for f in local_list:
                if f not in remote_list:
                    print "file missed, uploading ... " + f
                    file_key = self.aws_key_path + f
                    self.push_picture_to_s3(file_key, '/home/gopro/' + self.info.get('name') + "/" + f)

            # now, all pictures are uploaded, so delete all
            for f in local_list:
                utils.delete_file('/home/gopro/' + self.info.get('name') + "/" + f)

            return True
        except ValueError as e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False

    def push_picture_to_s3(self, file_key, file_path):
        try:
            # connect to the bucket
            conn = boto.connect_s3(aws_key, aws_security)
            bucket = conn.get_bucket(aws_bucket)

            print "Uploading to AWS.. Key: ", file_key, "  Path: ", file_path

            k = Key(bucket)
            k.key = file_key
            k.set_contents_from_filename(file_path)
            # we need to make it public so it can be accessed publicly
            # using a URL like http://s3.amazonaws.com/bucket_name/key
            k.make_public()
        except ValueError as e:
            print e

    def get_remote_file_list(self, key_path):
        # connect to the bucket
        conn = boto.connect_s3(aws_key, aws_security)
        bucket = conn.get_bucket(aws_bucket)
        # go through the list of files
        bucket_list = bucket.list()
        re = []
        for l in bucket_list:
            f_name = str(l.key)
            if f_name.startswith(key_path) and f_name != key_path:
                re.append(f_name)

        return re

    def sync_image_txt_file(self, new_file_name):
        key_name = self.aws_key_path + 'image.txt'
        conn = boto.connect_s3(aws_key, aws_security)
        bucket = conn.get_bucket(aws_bucket)
        item = bucket.get_key(key_name)
        if item is None:
            image_list = []
        else:
            image_list = item.get_contents_as_string().decode('utf-8').split()

        print "Original List: ", image_list
        image_list.append(new_file_name)
        if len(image_list) > 100:
            image_list.pop(0)
        item = Key(bucket)
        item.key = key_name
        item.set_contents_from_string("\n".join(image_list))

if __name__ == '__main__':

    ctrl = GoProCTRL(0)

    ctrl.sync_image_txt_file('test')
