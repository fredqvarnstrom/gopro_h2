# Prepare RPi

## Initialize RPi.
    
Open the Raspberry Pi config app by typing `sudo raspi-config` and perform follows.

- Expand file system.
- Set Time zone and date & time.

## Update packages.

    sudo apt-get update
    sudo apt-get upgrade
    sudo pip install sendgrid
    
Install packages for ADS1015 by following this:

https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/ads1015-slash-ads1115


# Adding cameras.
    
Open the `config.py` and add information about new camera.

Create interface file and place it to the `interface` directory.

NOTE: Make sure that interface file name is same with `interface` value in the `config.py`.

# Working with DropBox

    sudo pip install dropbox
    
