#!/usr/bin/env python
import httplib
import ftplib
import subprocess
from ftplib import FTP
import os, time, sys, glob, re
import urllib2
import logging
import datetime
import json
import url_H2
import url_H4
import utils
import config
import boto
from boto.s3.key import Key
import socket
import dropbox
from dropbox_downloader import DropBoxDownloader

# ###########################    Initialization   ################################################

ftp_server_ip = config.FTP_SERVER_IP
ftp_id = config.FTP_ID
ftp_pwd = config.FTP_PWD

logging_level = int(utils.get_param_from_xml('LOG_LEVEL'))

aws_bucket = utils.get_param_from_xml('AWS_BUCKET_NAME')
aws_key = utils.get_param_from_xml('AWS_KEY')
aws_security = utils.get_param_from_xml('AWS_SECRET')
host_name = socket.gethostname()

time_now = datetime.datetime.now()
log_file_name = '/home/gopro/log/' + time_now.strftime("%Y-%m-%d") + ".txt"

inst_dropbox = DropBoxDownloader(utils.get_param_from_xml('DROPBOX_TOKEN'))

b_dropbox = True if utils.get_param_from_xml('ENABLE_DROPBOX') == 'ON' else False


class GoProCTRL:

    info = None         # contains all info about camera
    c_type = None       # Camera type,    H2 or H3 or H4
    jpg_dir = ''

    url_base = None
    aws_key_path = ''

    def __init__(self, num):
        self.info = config.GoPro_List[num]
        self.c_type = self.info.get('type')
        if self.c_type == "H4":
            self.url_base = url_H4
        else:
            self.url_base = url_H2

        self.jpg_dir = '/home/gopro/' + self.info.get('name') + "/"
        if not os.path.exists(self.jpg_dir):
            os.makedirs(self.jpg_dir)
        self.aws_key_path = host_name + "/" + self.info.get('name') + "/"

    def wake(self):
        print "wake up GoPro..."
        logging.info("wake up GoPro...")
        if self.c_type == "H4":
            return self.send_cmd(self.url_base.url_shutter_on)
        else:
            return self.send_cmd(self.url_base.url_gopro_on)

    def send_cmd(self, cmd):
        if self.c_type == "H4":
            url = config.GOPRO_URL + cmd
        else:
            url = config.GOPRO_URL + cmd.format(self.info.get('pwd'))

        print "Sending command,   ", url

        try:
            result = urllib2.urlopen(url).read()
            return True
        except urllib2.HTTPError, e:
            logging.error('HTTPError = ' + str(e.code))
            return False
        except urllib2.URLError, e:
            logging.error('URLError = ' + str(e.reason))
            return False
        except httplib.HTTPException, e:
            logging.error('HTTPException')
            return False
        except:
            import traceback
            logging.error('Generic exception: ' + traceback.format_exc())
            return False

    def sleep(self):
        print "- sleep"
        logging.info("- sleep")
        return self.send_cmd(self.url_base.url_gopro_off)

    def delete_all(self):
        print "-- Deleting all photos."
        logging.info("- Deleting all")
        return self.send_cmd(self.url_base.url_delete_all)

    def takepic(self):
        print "- take a photo"
        logging.info("- take a photo")
        if not self.send_cmd(self.url_base.url_mode_photo):
            return False
        utils.count_down(5)  # wait for photo mode to turn on
        if not self.send_cmd(self.url_base.url_shutter_on):
            return False
        utils.count_down(1)  # wait for photo to be taken
        return True

    def set_date_time(self):
        print "setting date and time..."
        logging.info("setting date and time...")
        s = time.strftime("%y%%%m%%%d%%%H%%%M%%%S", time.localtime())
        url = self.url_base.url_set_date_time + s
        return self.send_cmd(url)

    def download(self, last=True):
        print "- download last one"
        try:
            logging.info("- download last one")
            url = config.GOPRO_URL + config.url_media

            result = urllib2.urlopen(url).read()
            dirs = re.findall('href="(\d\d\dGOPRO)/"', result)
            if not dirs:
                print "No Media Folders"
                logging.error("No Media Folders")
                return None

            url += "/" + dirs[-1]
            result = urllib2.urlopen(url).read()
            pics = re.findall('href="(GOPR\d+\.JPG)"', result)
        except urllib2.URLError as e:
            logging.error(e)
            print e
            return None

        if not pics:
            print "No Pictures"
            logging.error("No Pictures")
            return None

        def download_pic(url, pic):
            url += "/" + pic
            result = urllib2.urlopen(url)
            print "Downloading %s (%s bytes)..." % (pic, result.headers['content-length'])
            logging.info("Downloading %s (%s bytes)..." % (pic, result.headers['content-length']))
            dir_path = '/home/gopro/' + self.info.get('name') + "/"

            download_file_name = dir_path + pic
            with open(download_file_name, "wb") as f:
                while True:
                    chunk = result.read(16 * 1024)
                    if not chunk:
                        break
                    f.write(chunk)
                    print f.tell(), "\r",
                    sys.stdout.flush()
            return download_file_name

        if last:
            return download_pic(url, max(pics))
        else:
            return [download_pic(url, pic) for pic in pics]

    def delete(self, last=True):
        print "- delete"
        logging.info("- delete")
        return self.send_cmd(self.url_base.url_delete_last if last else self.url_base.url_delete_all)

    def upload_file(self, photo_file_name, dr=None):
        try:
            p_file = open(photo_file_name, 'rb')
        except IOError as e:
            print 'Cannot upload file. Maybe file is corrupt or damaged'
            return False

        try:
            print 'File uploading : ' + photo_file_name
            logging.info('Uploading file : ' + photo_file_name)
            # insert exception code when failed to connect to the FTP server
            ftp = FTP(ftp_server_ip)  # the domain name is "ftp.comtechnology.co.nz"
            # insert exception code when failed to log in to the FTP server
            ftp.login(ftp_id, ftp_pwd)

            if dr is not None:
                if self.info.get('name') not in ftp.nlst():
                    ftp.mkd(self.info.get('name'))
                ftp.cwd(self.info.get('name'))

            # can change the file name when uploading
            ftp.storbinary('STOR ' + photo_file_name.split('/')[-1], p_file)
            print "STORing File now..."
            logging.info("STORing File now...")
            ftp.quit()
            p_file.close()
            print "File transfered"
            logging.info("File transfered")
            logging.info("")
            return True
        except ftplib.all_errors, e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False

    def sync_img(self):
        try:
            ftp = FTP(ftp_server_ip)  # the domain name is "ftp.comtechnology.co.nz"
            ftp.login(ftp_id, ftp_pwd)
            ftp.cwd(self.info.get('name'))

            l_list = glob.glob("/home/gopro/" + self.info.get('name') + "/*.JPG")
            local_list = []
            for f in l_list:
                buf = f.split('/')
                local_list.append(buf[-1])

            p_list = self.get_remote_file_list(self.aws_key_path)

            remote_list = [os.path.basename(f_path) for f_path in p_list]

            print "local_list : ", local_list
            print "remote_list : ", remote_list

            # if picture does not exist on AWS S3, upload it
            for f in local_list:
                if f not in remote_list:
                    msg = "file missed, uploading ... " + f
                    print msg
                    logging.info(msg)
                    self.upload_file('/home/gopro/' + self.info.get('name') + "/" + f, True)

            # now, all pictures are uploaded, so delete all
            for f in local_list:
                utils.delete_file('/home/gopro/' + self.info.get('name') + "/" + f)

            return True
        except ftplib.all_errors, e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False

    def sync_with_aws(self):
        try:
            l_list = glob.glob("/home/gopro/" + self.info.get('name') + "/*.JPG")
            local_list = [os.path.basename(f_path) for f_path in l_list]

            l_a_list = self.get_remote_file_list(self.aws_key_path)
            remote_list = [os.path.basename(f_path) for f_path in l_a_list]

            print "local_list : ", local_list
            print "remote_list : ", remote_list
            logging.info('Get local/remote list to sync with AWS...')
            # if picture does not exist on FTP server, upload it
            for f in local_list:
                if f not in remote_list:
                    msg = "file missed, uploading ... " + f
                    print msg
                    logging.info(msg)
                    file_key = self.aws_key_path + f
                    self.push_picture_to_s3(file_key, '/home/gopro/' + self.info.get('name') + "/" + f)

            # now, all pictures are uploaded, so delete all
            for f in local_list:
                utils.delete_file('/home/gopro/' + self.info.get('name') + "/" + f)

            logging.info("Succeeded to sync with AWS...")

            return True
        except ValueError as e:
            errorcode_string = str(e).split(None, 1)
            logging.error(errorcode_string)
            return False
        except socket.gaierror as e:
            print e
            logging.error(e)
            return False

    def push_picture_to_s3(self, file_key, file_path):
        try:
            # connect to the bucket
            conn = boto.connect_s3(aws_key, aws_security)
            bucket = conn.get_bucket(aws_bucket)

            msg = "Uploading to AWS.. Key: " + file_key + "  Path: " + file_path
            print msg
            logging.info(msg)

            k = Key(bucket)
            k.key = file_key
            k.set_contents_from_filename(file_path)
            # we need to make it public so it can be accessed publicly
            # using a URL like http://s3.amazonaws.com/bucket_name/key
            k.make_public()
            print "Succeeded to upload to AWS S3..."
            logging.info("Succeeded to upload to AWS S3...")
            return True

        except ValueError as e:
            print e
            logging.error(e)
            return False
        except socket.gaierror as e:
            print e
            logging.error(e)
            return False
        except boto.exception.S3ResponseError as e:
            print e
            logging.error(e)
            return False

    def get_remote_file_list(self, key_path):
        try:
            # connect to the bucket
            conn = boto.connect_s3(aws_key, aws_security)
            bucket = conn.get_bucket(aws_bucket)
            # go through the list of files
            bucket_list = bucket.list()
            re = []
            for l in bucket_list:
                f_name = str(l.key)
                if f_name.startswith(key_path) and f_name != key_path:
                    re.append(f_name)

            return re
        except ValueError as e:
            print e
            logging.error(e)
            return False
        except socket.gaierror as e:
            print e
            logging.error(e)
            return False

    def sync_image_txt_file(self, new_file_name):
        try:
            print "Updating image.txt now..."
            key_name = self.aws_key_path + 'image.txt'
            conn = boto.connect_s3(aws_key, aws_security)
            bucket = conn.get_bucket(aws_bucket)
            item = bucket.get_key(key_name)
            if item is None:
                image_list = []
            else:
                image_list = item.get_contents_as_string().decode('utf-8').split()

            # print "Original List: ", image_list
            image_list.append(new_file_name)
            if len(image_list) > 100:
                image_list.pop(0)
            item = Key(bucket)
            item.key = key_name
            item.set_contents_from_string("\n".join(image_list))
            logging.info("Succeeded to update image.txt")
        except ValueError as e:
            print e
            logging.error(e)
            return False
        except socket.gaierror as e:
            print e
            logging.error(e)
            return False


def main():

    for i in range(len(config.GoPro_List)):
        ctrl = GoProCTRL(i)

        inst = config.GoPro_List[i]

        print "---------------------  ", inst.get('name'), "  --------------------------"

        utils.run_sh(inst.get('interface'))

        if not ctrl.wake():
            msg = "failed to wake up " + inst.get('name')
            print msg
            logging.error(msg)
            # switch back to internet and return
            if utils.get_param_from_xml('SWITCH_INTERNET') == 'ON':
                utils.run_sh(config.wifi_interface)
            msg = "Dear Sir\n " + host_name + " has " + msg + " at " + \
                  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            utils.send_warning_mail(utils.get_param_from_xml("WARNING_MAIL_TO"), "Dear Sir\n" + msg)
            continue

        # Check last formatted time and delete all images per month
        if int(utils.get_param_from_xml('LAST_FORMAT')) != datetime.datetime.now().month:
            if ctrl.delete_all():
                utils.set_param_to_xml('LAST_FORMAT', str(datetime.datetime.now().month))

        time.sleep(10)
        if not ctrl.takepic():
            p_file_name = False
        else:
            time.sleep(10)
            p_file_name = ctrl.download()

        if not p_file_name:
            msg = "failed to download last image from the " + inst.get('name')
            print msg
            logging.error(msg)
            msg = "Dear Sir\n " + host_name + " has " + msg + " at " + \
                  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            utils.send_warning_mail(utils.get_param_from_xml("WARNING_MAIL_TO"), "Dear Sir\n" + msg)
            continue

        ctrl.delete()
        time.sleep(5)
    #    set_date_time()
        time.sleep(5)
        ctrl.sleep()
        time.sleep(5)

        if utils.get_param_from_xml('SWITCH_INTERNET') == 'ON':
            utils.run_sh(config.wifi_interface)

        file_key = host_name + "/" + inst.get('name') + "/" + os.path.basename(p_file_name)

        if b_dropbox:
            inst_dropbox.connect()
            inst_dropbox.create_folder('/'.join(['', host_name, inst.get('name')]))
            inst_dropbox.upload_file(file_key, p_file_name)
            os.remove(p_file_name)

        if not b_dropbox:
            if not ctrl.push_picture_to_s3(file_key, p_file_name):
                msg = "Failed to upload to the AWS... Will try again afterwards."
                print msg
                logging.info(msg)
                msg = "Dear Sir\n " + host_name + " has " + msg + "    at " + \
                      datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                utils.send_warning_mail(utils.get_param_from_xml("WARNING_MAIL_TO"), "Dear Sir\n" + msg)

            ctrl.sync_with_aws()

            ctrl.sync_image_txt_file(os.path.basename(p_file_name))

    # utils.run_sh(config.wifi_interface)

    solar_volt = utils.read_adc()

    if not b_dropbox:
        if not GoProCTRL(0).push_picture_to_s3(host_name + '/adc_log.csv', '/home/gopro/adc_log.csv'):
            print "Failed to upload adc log file to AWS... Will retry afterwards."
            logging.info("Failed to upload adc log file to AWS... Will retry afterwards.")
            return False

        GoProCTRL(0).push_picture_to_s3(host_name + '/' + os.path.basename(log_file_name), log_file_name)
    else:
        inst_dropbox.connect()
        try:
            inst_dropbox.client.file_delete(host_name + '/adc_log.csv')
        except dropbox.rest.ErrorResponse as e:
            print e
            pass
        inst_dropbox.upload_file(host_name + '/adc_log.csv', '/home/gopro/adc_log.csv')
        try:
            inst_dropbox.client.file_delete(host_name + '/' + os.path.basename(log_file_name))
        except dropbox.rest.ErrorResponse as e:
            print e
            pass
        inst_dropbox.upload_file(host_name + '/' + os.path.basename(log_file_name), log_file_name)

    if solar_volt < 11.5:
        msg = "Shutting down due to low Voltage at " + str(solar_volt)
        logging.error(msg)
        print msg
        msg = "Dear Sir\n " + host_name + " is " + msg + " at " + \
              datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        utils.send_warning_mail(utils.get_param_from_xml("WARNING_MAIL_TO"), "Dear Sir\n" + msg)

        print "Syncing file system, then we're shutting down."
        os.system("sync")

        cmd = "/usr/bin/sudo /sbin/shutdown -h now"
        proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        output = proc.communicate()[0]
        print output

    return True


if __name__ == '__main__':

    logging.basicConfig(level=logging_level, filename=log_file_name, format='%(asctime)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Script started")

    logging.getLogger('boto').setLevel(logging.CRITICAL)
    logging.getLogger('Adafruit_I2C').setLevel(logging.CRITICAL)

    if not utils.check_internet():
        # get yesterday's date & time and set new date & time
        str_old_time = utils.get_param_from_xml('DATETIME')
        if not str_old_time:
            str_old_time = "2016-01-01 07:55:00"
        old_time = datetime.datetime.strptime(str_old_time, "%Y-%m-%d %H:%M:%S")
        time_now = (old_time + datetime.timedelta(days=1)).replace(hour=7, minute=55)
        str_time = time_now.strftime("%Y-%m-%d %H:%M:%S")
        logging.info("Internet connection is lost, setting time as %s" % str_time)
        os.system("date --set='" + str_time + "'")

    if utils.get_param_from_xml('SCHEDULE') == 'server':
        interval, str_shutdown_time = utils.get_interval_shutdown_time_from_server()
    else:
        interval = int(utils.get_param_from_xml('INTERVAL'))
        str_shutdown_time = utils.get_param_from_xml('SHUTDOWN_TIME')

    tmp = str_shutdown_time.split(':')

    sh_hour = int(tmp[0])
    sh_min = int(tmp[1])

    if b_dropbox:
        inst_dropbox.connect()
        inst_dropbox.create_folder('/' + host_name)

    print "Synchronizing date & time..."
    utils.sync_time()
    print ""

    while True:
        time_now = datetime.datetime.now()
        print "Time: ", time_now
        if 1 == 2:
            pass
        # if time_now.hour >= sh_hour and time_now.minute >= sh_min:  # turn off after 22:00
            # print "Stopping work now..."
            # time.sleep(5)
            # logging.info("Saving time, %s" % time_now.strftime("%Y-%m-%d %H:%M:%S"))
            # utils.save_current_datetime()
            # os.system("shutdown -h now")

        else:  # start work after 0 AM
            print "Starting..."
            s_time = time.time()
            if not main():
                msg = "Dear Sir\n " + host_name + " has failed to work at " + \
                      datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                utils.send_warning_mail(utils.get_param_from_xml("WARNING_MAIL_TO"), "Dear Sir\n" + msg)
            while time.time() - s_time < interval * 60:
                time.sleep(1)
