import csv
import os
import traceback
import urllib2
import xml.etree.ElementTree
import sys
import time
import datetime
import sendgrid
from sendgrid.helpers.mail import *
import platform

try:
    import Adafruit_ADS1x15
    adc = Adafruit_ADS1x15.ADS1015()

except ImportError:
    print 'Failed to import ADS1x15 package'

if platform.system() == 'Windows':
    conf_file_name = 'config.xml'
else:
    conf_file_name = "/home/gopro/config.xml"

adc_log_file_name = '/home/gopro/adc_log.csv'
ADC_GAIN = 1


def check_internet():
    try:
        response = urllib2.urlopen('https://www.google.co.nz', timeout=1)
        return True
    except urllib2.URLError as err:
        pass
    return False


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break

    return tmp


def set_param_to_xml(tag_name, new_val):
    et = xml.etree.ElementTree.parse(conf_file_name)
    for child_of_root in et.getroot():
        if child_of_root.tag == tag_name:
            child_of_root.text = new_val
            et.write(conf_file_name)
            return True
    return False


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


def run_sh(if_name):

    os.system("ifdown wlan0")
    time.sleep(5)
    script = "cp /home/gopro/interface/" + if_name + " /etc/network/interfaces"
    print "SHELL SCRIPT: ", script
    os.system(script)

    time.sleep(2)
    os.system("ifup wlan0")
    time.sleep(10)

    print "Shell script running completed"
    # count_down(30)
    time.sleep(10)
    return True


def delete_file(file_name):
    """
    Delete file
    :param file_name:
    :return:
    """
    print "Deleting old file (", file_name, ")"
    try:
        os.remove(file_name)
    except IOError as e:
        print 'Cannot remove file. Maybe file is corrupt or damaged'
        return False
    except OSError as e:
        print 'Cannot remove file. Maybe file is corrupt or damaged'
        return False


def get_remote_file_list(ftp_connection):
    files = []

    def dir_callback(line):
        bits = line.split()
        if 'd' not in bits[0]:
            files.append(bits[-1])

    ftp_connection.dir(dir_callback)

    f_list = []
    for i in range(len(files)):
        f = files[i]
        if f.count('.') > 0:  # then file has its extension
            if f.split('.')[1] == 'JPG':
                f_list.append(f)

    return f_list


def sync_time():
    os.system("/etc/init.d/ntp stop")
    os.system("ntpd -qg")
    time.sleep(3)
    os.system("/etc/init.d/ntp start")


def save_current_datetime():
    """
    Save current datetime to the xml file
    :return:
    """
    cur_time = datetime.datetime.now()
    str_time = cur_time.strftime("%Y-%m-%d %H:%M:%S")
    set_param_to_xml("DATETIME", str_time)


def read_adc():

    val0 = adc.read_adc(0, gain=ADC_GAIN)
    val1 = adc.read_adc(1, gain=ADC_GAIN)
    val2 = adc.read_adc(2, gain=ADC_GAIN)
    volt = float(val0) / 100.0
    solar = float(val2) / 100.0 + 4.9

    # convert voltage to temperature
    vol1 = val1 * 4.096 / 2048
    temp = 25 + (vol1 - 0.75) * 100

    print "Battery voltage is : ", volt, "V"
    print "Solar voltage is : ", solar, "V"
    print "Temperature from TMP36 is : ", temp, "C"
    print ""

    time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    f_exist = False
    if os.path.exists(adc_log_file_name):
        f_exist = True

    m_file = open(adc_log_file_name, 'a')
    wr = csv.writer(m_file, quoting=csv.QUOTE_MINIMAL)

    print f_exist
    if not f_exist:
        wr.writerow(['Date & Time', 'Voltage', 'Temperature'])
    wr.writerow([time_str, volt, temp])

    m_file.close()

    return volt


def send_mail(reciptent, message):
    """
    Send mail.
    """
    print "Sending warining email..."
    sg = sendgrid.SendGridAPIClient(apikey="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")
    from_email = Email("client1@comtechnology.co.nz")

    subject = "Mail from GoPro Client"

    to_email = Email(reciptent)

    content = Content("text/plain", message)

    mail = Mail(from_email, subject, to_email, content)

    response = sg.client.mail.send.post(request_body=mail.get())

    print(response.status_code)
    print(response.body)
    print(response.headers)
    return True


def send_warning_mail(recip, msg):
    """
    Check last sent time of warning mail.
    If it was a days ago, sent mail and update it to ensure sending email a time in a day.
    """
    str_last_sent = get_param_from_xml("LAST_MAIL_SENT")

    last_sent = datetime.datetime.strptime(str_last_sent, "%Y-%m-%d %H:%M:%S")

    if (datetime.datetime.now() - last_sent).days > 0:
        send_mail(recip, msg)
        time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        set_param_to_xml("LAST_MAIL_SENT", time_str)
        return True

    else:
        return False


def get_interval_shutdown_time_from_server():
    url = 'http://' + get_param_from_xml('SERVER_URL') + '/get_schedule'
    req = urllib2.Request(url)
    try:
        resp = urllib2.urlopen(req)
        sch_data = json.loads(resp.read())
        day_of_week = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'][datetime.datetime.today().weekday()]
        cur_sch = sch_data[day_of_week]
        return int(cur_sch['interval']), cur_sch['stop']

    except:
        print "Failed to send request."
        traceback.print_exc()
        return None


if __name__ == '__main__':
    print get_interval_shutdown_time_from_server()